console.log("Server starting...");
const express = require("express");
const bodyParser = require("body-parser"); 
const path = require("path"); 
const mysql = require("mysql");
const session = require("express-session");
const uuid = require("uuid/v4"); //using uuid/v4 - random

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const PORT = process.env.PORT || 3000;

const pool = mysql.createPool({
    host: "localhost", port: "3306",
    user: "David", password: "highway1",
    database: "grocery",
    connectionLimit: 4
});

app.use(express.static(__dirname + "/../client/"));

//INSERT PRODUCT
const SQL_INSERT_PRODUCT = "INSERT INTO GROCERY_LIST (ID, UPC12, BRAND, NAME) VALUES (?, ?, ?, ?)"

app.post("/grocerieslist", function(req, res){
    console.info("req.body is: ", req.body);
    var placeholderID = 6969;
    pool.getConnection(function(err,conn){
        if (err){
            console.error("Error connecting to DB: ", err)
            res.status(500);
            return;
        }
        conn.query(SQL_INSERT_PRODUCT,
            [placeholderID, req.body.upc12, req.body.brand, req.body.name],
            function (err, result){
                conn.release();
                if (err){
                console.error("Error when posting to database: ", err)
                return;
                }
                res.status(202)
                console.info("Item data submitted to DB.")
        })
    })
});

//EDIT PRODUCT
const SQL_EDIT_PRODUCT = "UPDATE GROCERY_LIST SET BRAND = ?, NAME = ?, UPC12 = ? WHERE NAME = ? "

app.put("/grocerieslist", function(req, res){
    console.info("req.body is: ", req.body);
    pool.getConnection(function(err,conn){
        if (err){
            console.error("Error connecting to DB: ", err)
            res.status(500);
            return;
        }
        conn.query(SQL_EDIT_PRODUCT,
            [req.body.brand, req.body.name, req.body.upc12, req.body.oldname],
            function (err, result){
                conn.release();
                if (err){
                console.error("Error when posting to database: ", err)
                return;
                }
                res.status(202)
                console.info("Item data submitted to DB.")
        })
    })
});

//GET FIRST 20 ITEMS by A-Z
const SQL_GET_FIRST_20_PRODUCTS_AZ = "SELECT * FROM GROCERY_LIST ORDER BY NAME LIMIT 20"

app.get("/grocerieslist", function(req,res){
    console.info("Retrieving items from DB");
    pool.getConnection(function(err, conn){
        if (err){
            console.error("Error connecting to DB: ", err)
            return;
        }
        conn.query(SQL_GET_FIRST_20_PRODUCTS_AZ, function(err,result){
            if (err) {
                console.error("Error retreiving item data ", err);
                conn.release();
                return;
            }
            console.log("Data received is: ", JSON.stringify(result))
            res.type("application/json");
            res.status(200);
            res.json(result);
            conn.release();
        })
    })
});

//GET FIRST 20 ITEMS by Z-A
const SQL_GET_FIRST_20_PRODUCTS_ZA = "SELECT * FROM GROCERY_LIST ORDER BY NAME DESC LIMIT 20"

app.get("/grocerieslistZA", function(req,res){
    console.info("Retrieving items from DB");
    pool.getConnection(function(err, conn){
        if (err){
            console.error("Error connecting to DB: ", err)
            return;
        }
        conn.query(SQL_GET_FIRST_20_PRODUCTS_ZA, function(err,result){
            if (err) {
                console.error("Error retreiving item data ", err);
                conn.release();
                return;
            }
            console.log("Data received is: ", JSON.stringify(result))
            res.type("application/json");
            res.status(200);
            res.json(result);
            conn.release();
        })
    })
});



//SEARCH ITEMS
const SQL_SEARCH_ITEMS = "SELECT * FROM GROCERY_LIST WHERE NAME OR BRAND LIKE ?"

app.get("/grocerieslist/:search", function(req,res){
    console.info("Retrieving items from DB");
    console.info("req.params.search:", req.params.search)
    pool.getConnection(function(err, conn){
        if (err){
            console.error("Error connecting to DB: ", err)
            return;
        }
        conn.query(SQL_SEARCH_ITEMS, ['%' + req.params.search + '%'], function(err,result){
            if (err) {
                console.error("Error retrieving item data ", err);
                conn.release();
                return;
            }
            console.log("Data received is: ", JSON.stringify(result))
            res.type("application/json");
            res.status(200);
            res.json(result);
            conn.release();
        })
    })
});

app.use(function(req, res) {
    console.log("404 Error.");
    res.send("<h1>404 Error. Please try another door.</h1>");
});

app.listen(PORT, function() {
    console.log("Server started at " + PORT);
});