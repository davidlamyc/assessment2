-- ============================

-- This file was created using Derby's dblook utility.
-- Timestamp: 2016-03-01 20:57:18.109
-- Source database is: sample
-- Connection URL is: jdbc:derby://localhost:1527/sample;user=app;password=app
-- Specified schema is: app
-- appendLogs: false

-- ----------------------------------------------
-- DDL Statements for tables
-- ----------------------------------------------

drop schema if exists lab_db;

create schema lab_db;

use lab_db;


CREATE TABLE ITEM (
ITEM_ID INTEGER NOT NULL,
CATEGORY VARCHAR(14) NOT NULL,
NAME VARCHAR(20) NOT NULL,
DESCRIPTION VARCHAR(255),
RATE DECIMAL(13,2)
)


INSERT INTO ITEM (ITEM_ID, CATEGORY, NAME, DESCRIPTION, RATE) 
	VALUES (0000001, 'travel', 'backpack', 'new herschel backpack', '10.0');
INSERT INTO ITEM (ITEM_ID, CATEGORY, NAME, DESCRIPTION, RATE) 
	VALUES (0000002, 'travel', 'torchlight', 'works on AAA battteries', '5.0');
INSERT INTO ITEM (ITEM_ID, CATEGORY, NAME, DESCRIPTION, RATE) 
	VALUES (0000003, 'music', 'Fender Telecaster', 'bought in 1999, good condition', '50.0');        
 
-- "travel" "books" "electrical" "music" 