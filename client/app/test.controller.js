(function () { //IIFE used -> client side 
    "use strict";

    var GroceryApp = angular.module("GroceryApp",["ui.router"]);

//CONFIG-----------------------------------------------------------

    var GroceryConfig = function($stateProvider, $urlRouterProvider){

        $stateProvider
            .state("search",{
                url:"/search",
                templateUrl: "/views/search.html",
            })
            .state("edit",{
                url:"/edit",
                templateUrl:"/views/edit.html",
            })
            .state("add",{
                url:"/add",
                templateUrl:"/views/add.html",
            });

        $urlRouterProvider.otherwise("/search");
    }

    GroceryConfig.$inject = [ "$stateProvider", "$urlRouterProvider"];

//CONTROLLERS------------------------------------------------------------
    var GroceryCtrl = function($http,$q,$httpParamSerializeJQLike,$state) {
        var groceryctrl = this; // vm, this for the controller to point to this variable. 

        groceryctrl.products = [];

        groceryctrl.product = {};

        //search for product
        groceryctrl.searchProducts = function(){
            console.log("Searching products from DB by", groceryctrl.search);
            var defer = $q.defer();
            $http.get("/grocerieslist/" + groceryctrl.search)
                .then(function(result){
                    console.log("Result received.")
                    groceryctrl.products = result.data;
                    console.log("groceryctrl.products is currently:", groceryctrl.products)
                })
                .catch(function(err){
                    console.log("Error retrieving search products.")
                })
        };

        //initialise product list A-Z
        groceryctrl.initProducts = function(){
            console.info("Getting item from DB.");
            var defer = $q.defer();
            $http.get("/grocerieslist")
                .then(function(result){
                    console.log("Result received.")
                    groceryctrl.products = result.data;
                    console.log("groceryctrl.products is currently:", groceryctrl.products)
                })
                .catch(function(err){
                    console.log("Error retrieving data.")
                })
            }
        groceryctrl.initProducts();

        //Get product list Z-A
        groceryctrl.initProductsZA = function(){
            console.info("Getting item from DB.");
            var defer = $q.defer();
            $http.get("/grocerieslistZA")
                .then(function(result){
                    console.log("Result received.")
                    groceryctrl.products = result.data;
                    console.log("groceryctrl.products is currently:", groceryctrl.products)
                })
                .catch(function(err){
                    console.log("Error retrieving data.")
                })
            }
        groceryctrl.initProducts();

        //init success message
        groceryctrl.submitMsg = "";

        //Add product
        groceryctrl.addProduct = function (){
            console.log("Adding item: ", groceryctrl.product);
            $http({
                url: "/grocerieslist",
                method: "POST",
                data: $httpParamSerializeJQLike({
                    brand: groceryctrl.product.brand,
                    name: groceryctrl.product.name,
                    upc12: groceryctrl.product.upc12
                }),
                headers:{
                        "Content-Type": "application/x-www-form-urlencoded"
                }
            })
            .then(function(result) {
                console.info("Posting product to server...");
                res.status(200);
                res.send(result);
            })
            .catch(function (e) {
                console.error("Error sending product data to server...")
                console.log(e);
            });
            groceryctrl.submitMsg = "Product submitted!"
        }

    //go to edit page
        groceryctrl.gotoedit = function(index){
            //stores old name that will be used to search
            groceryctrl.product.oldname = groceryctrl.products[index].name;
            //edited fields
            groceryctrl.product.name = groceryctrl.products[index].name;
            groceryctrl.product.brand = groceryctrl.products[index].brand;
            groceryctrl.product.upc12 = groceryctrl.products[index].upc12;

            console.log("Product item name to be changed:", groceryctrl.product.name);
            console.log("Product brand name to be changed:", groceryctrl.product.brand);
            console.log("Product upc12 to be changed:", groceryctrl.product.upc12);
            $state.go("edit");
        }

    //edit product
        groceryctrl.editProduct = function(index){
            console.info("Editing product...")
            console.log("old item name:", groceryctrl.product.oldname);
            console.log("new name:", groceryctrl.product.name);
            console.log("new brand:", groceryctrl.product.brand);
            console.log("new upc12:", groceryctrl.product.upc12);
            $http({
                url: "/grocerieslist",
                method: "PUT",
                data: $httpParamSerializeJQLike({
                    oldname: groceryctrl.product.oldname,
                    name: groceryctrl.product.name,
                    brand: groceryctrl.product.brand,
                    upc12: groceryctrl.product.upc12
                }),
                headers:{
                        "Content-Type": "application/x-www-form-urlencoded"
                }
            })
            .then(function(result) {
                console.info("Posting product to server...");
                res.status(200);
                res.send(result);
            })
            .catch(function (e) {
                console.error("Error sending product data to server...")
                console.log(e);
            });
            $state.go("search");
        }

    };

//SERVICES & CONTROLLERS LIST
    GroceryApp.config(["$stateProvider","$urlRouterProvider", GroceryConfig])
    GroceryApp.controller("GroceryCtrl", [ "$http", "$q", "$httpParamSerializerJQLike", "$state", GroceryCtrl ]); 
})();


